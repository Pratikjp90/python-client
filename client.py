import socket

clientHere = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

clientHere.connect(('', 8888))

msg = clientHere.recv(1024)
print(msg)

if len(msg) <= 0:
	print("no MSG")   # never actuallly runs this line
else:
	#print("got it!")
	print(msg)
	inp = input("Enter your husky Id: ")
	clientHere.sendall(bytes("HELLO " + str(inp) + "\n", "utf-8"))

newMsg = clientHere.recv(1024).decode("utf-8")

while True:
	data = newMsg
	print(data)
	maths, firstValue, operation, lastValue = data.split(' ', 3)

	firstValue = int(firstValue)
	lastValue = int(lastValue)

	ans = 0

	if operation == "+":
		#print(firstValue + lastValue)
		ans = (firstValue + lastValue)
	elif operation == "-":
		#print(firstValue - lastValue)
		ans = (firstValue - lastValue)
	elif operation == "*":
		#print(firstValue * lastValue)
		ans = (firstValue * lastValue)
	elif operation == "/":
		#print(firstValue / lastValue)
		ans = int(firstValue / lastValue)   # typecasting float value into int..
	else:
		#print(firstValue % lastValue)
		ans = (firstValue % lastValue)
	print("ans = "+ str(ans))

	finalAns = bytes("ANSWER " + str(ans) + "\n", "utf-8")
	print(finalAns)

	clientHere.sendall(finalAns)
	#print("here before")

	newMsg2 = clientHere.recv(1024).decode("utf-8")
	newMsg2 = str(newMsg2)

	#print("here here")
	print(newMsg2)

	if "DONE" in newMsg2:
		#print("broke free")
		break
	else:
		#print(newMsg2)
		newMsg = newMsg2

clientHere.close()
